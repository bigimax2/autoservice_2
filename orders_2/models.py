from django.db import models
from django.urls import reverse


class CarOrder(models.Model):
    TRANSMISSION_IN_CAR = [
        ('',''),
        ('МКПП', 'МКПП'),
        ('АКПП', 'АКПП'),
        ('РОБОТ', 'РОБОТ'),
        ('ВАРИАТОР', 'ВАРИАТОР'),
    ]
    ENGINE_IN_CAR = [
        ('', ''),
        ('Бензин', 'Бензин'),
        ('Дизель', 'Дизель'),
        ('Электро', 'Электро'),
    ]

    car_brand = models.CharField(max_length=255, blank=True, null=True, verbose_name='Марка авто')
    car_model = models.CharField(max_length=255, blank=True, null=True, verbose_name='Модель авто')
    car_reg_nun = models.CharField(max_length=255, blank=False, null=True, verbose_name='Рег.номер авто')
    car_transmission = models.CharField(choices=TRANSMISSION_IN_CAR, max_length=255, default='',
                                        verbose_name='Тип трансмиссии')
    car_engine = models.CharField(choices=ENGINE_IN_CAR, max_length=255, default='', verbose_name='Тип двигателя')
    car_owner = models.CharField(max_length=255, blank=False, null=True, verbose_name='Контакт для связи')
    car_owner_phone = models.CharField(max_length=12, blank=True, null=True, verbose_name='Телефон для связи')
    car_owner_email = models.EmailField(max_length=255, blank=True, null=True, verbose_name='email,опционно')
    car_comment = models.TextField(blank=True, null=True, verbose_name='Что болит ? Что лечить ?')
    date_order = models.DateField(auto_now_add=True,null=True, verbose_name='Дата заказа')

    class Meta:
        verbose_name = 'Заказы'
        verbose_name_plural = 'Заказы'

    def __str__(self):
        return self.car_reg_nun

    def get_absolute_url(self):
        return reverse('car:car_detail', kwargs={'pk': self.pk})


class JobEngine(models.Model):
    job_engine = models.CharField(max_length=255, blank=True, null=True, verbose_name='Работы по двигателю')
    price = models.IntegerField(null=True, verbose_name='Цена')
    job_file = models.FileField(blank=True, null=True, upload_to='media/')
    job_image = models.ImageField(blank=True, null=True, upload_to='media/')
    date_job = models.DateField(auto_now_add=True)
    car_order = models.ForeignKey(CarOrder, null=True, blank=True, on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Работы по двигателю'
        verbose_name_plural = 'Работы по двигателю'

    def __str__(self):
        return self.job_engine


class JobTransmission(models.Model):
    job_transmission = models.CharField(max_length=255, blank=True, null=True, verbose_name='Работы по КПП')
    price = models.IntegerField(null=True, verbose_name='Цена')
    job_file = models.FileField(blank=True, null=True, upload_to='media/')
    job_image = models.ImageField(blank=True, null=True, upload_to='media/')
    date_job = models.DateField(auto_now_add=True)
    car_order = models.ForeignKey(CarOrder, on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Работы по трансмиссии'
        verbose_name_plural = 'Работы по трансмиссии'

    def __str__(self):
        return self.job_transmission


class JobSuspended(models.Model):
    job_suspended = models.CharField(max_length=255, blank=True, null=True, verbose_name='Работы по подвеске')
    price = models.IntegerField(null=True, verbose_name='Цена')
    job_file = models.FileField(blank=True, null=True, upload_to='media/')
    job_image = models.ImageField(blank=True, null=True, upload_to='media/')
    date_job = models.DateField(auto_now_add=True)
    car_order = models.ForeignKey(CarOrder, on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Работы по подвеске'
        verbose_name_plural = 'Работы по подвеске'

    def __str__(self):
        return self.job_suspended


class JobCondition(models.Model):
    job_condition = models.CharField(max_length=255, blank=True, null=True, verbose_name='Работы по A/C')
    price = models.IntegerField(null=True, verbose_name='Цена')
    job_file = models.FileField(blank=True, null=True, upload_to='media/')
    job_image = models.ImageField(blank=True, null=True, upload_to='media/')
    date_job = models.DateField(auto_now_add=True)
    car_order = models.ForeignKey(CarOrder, on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Работы по кондиционеру'
        verbose_name_plural = 'Работы по кондиционеру'

    def __str__(self):
        return self.job_condition


class JobElectric(models.Model):
    job_electric = models.CharField(max_length=255, blank=True, null=True, verbose_name='Работы по электрике')
    price = models.IntegerField(null=True, verbose_name='Цена')
    job_file = models.FileField(blank=True, null=True, upload_to='media/')
    job_image = models.ImageField(blank=True, null=True, upload_to='media/')
    date_job = models.DateField(auto_now_add=True)
    car_order = models.ForeignKey(CarOrder, on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Работы по электрике'
        verbose_name_plural = 'Работы по электрике'

    def __str__(self):
        return self.job_electric


