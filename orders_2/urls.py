from django.urls import path
from . import views


urlpatterns = [
    path('', views.index, name='index'),
    path('allorders/', views.all_orders, name='allorders'),
    path('newform/', views.New_Orders, name='newform'),
    path('allorders/edite/<int:id>/', views.edite, name='edite'),
    path('allorders/edite_date_order/<int:id>/', views.edite_date_order, name='edite_date_order'),
    path('allorders/edite_date_order/<int:id>/jobs_engine/', views.create_job_engine, name='jobs_engine'),
    path('allorders/edite_date_order/<int:id>/jobs_transmission/', views.create_job_transmission, name='jobs_transmission'),
    path('allorders/edite_date_order/<int:id>/jobs_suspended/', views.create_job_suspended, name='jobs_suspended'),
    path('allorders/edite_date_order/<int:id>/jobs_condition/', views.create_job_condition, name='jobs_condition'),
    path('allorders/edite_date_order/<int:id>/jobs_electric/', views.create_job_electric, name='jobs_electric'),
]