from django.contrib import admin
from .models import CarOrder, JobEngine, JobTransmission, JobSuspended, JobCondition, JobElectric




@admin.register(CarOrder)
class CarOrderAdmin(admin.ModelAdmin):
    pass


@admin.register(JobEngine)
class JobEngineAdmin(admin.ModelAdmin):
    pass


@admin.register(JobTransmission)
class JobTransmissionAdmin(admin.ModelAdmin):
    pass


@admin.register(JobSuspended)
class JobSuspendedAdmin(admin.ModelAdmin):
    pass


@admin.register(JobCondition)
class JobConditionAdmin(admin.ModelAdmin):
    pass


@admin.register(JobElectric)
class JobElectricAdmin(admin.ModelAdmin):
    pass
