from django.apps import AppConfig


class Orders2Config(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'orders_2'
    verbose_name = 'Заказы по авто'
