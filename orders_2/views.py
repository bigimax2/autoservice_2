from django.http import HttpResponse
from django.shortcuts import render, redirect
from orders_2.forms import (Test,
                            JobEngineFormSet,
                            JobTransmissionFormSet,
                            JobFormSuspended, JobSuspendedFormSet, JobConditionFormSet, JobElectricFormSet,
                            )
from orders_2.models import CarOrder, JobEngine, JobTransmission, JobSuspended, JobCondition, JobElectric


# Рендер главной страницы
def index(request):
    return render(request, 'index.html')


# рендер всех заказов
def all_orders(request):
    all = CarOrder.objects.all()
    return render(request, 'allorders.html', {'all': all})


# рендер нового заказа
def New_Orders(request):
    if request.method == 'POST':
        form = Test(request.POST)
        if form.is_valid():
            try:
                CarOrder.objects.create(**form.cleaned_data)
                return redirect('allorders')
            except:
                form.add_error(None,'Ошибка заполнения')
    else:
        form = Test()
    return render(request, 'newform.html', {'form': form})


# редактирование данных о машине
def edite(request, id):
    model = CarOrder.objects.get(id=id)
    form = Test(instance=model)
    if request.method == 'POST':
        forms = Test(request.POST, instance=model)
        if forms.is_valid():
            table = forms.save()
            table.save()
            return redirect('allorders')
        else:
            return HttpResponse('НЕ удачно')

    return render(request, 'edite.html', {'form': form, 'id': id})


# рендерит данные о заказе
def edite_date_order(request, id):
    model2 = CarOrder.objects.get(id=id)

    car_brand = model2.car_brand
    car_model = model2.car_model
    reg_num = model2.car_reg_nun
    engine = model2.car_engine
    transmission = model2.car_transmission
    phone_number = model2.car_owner_phone
    customer = model2.car_owner
    email = model2.car_owner_email
    anamnes = model2.car_comment
    context = [car_brand, car_model, reg_num, engine, transmission,
               phone_number, customer, email, anamnes]

    return render(request, 'edite_jobs.html', { "context": context, 'id': id, 'reg_num': reg_num})


# рендерит работы по двигателю
def create_job_engine(request, id):
    carorder = CarOrder.objects.get(id=id)
    jobs = JobEngine.objects.filter(car_order=carorder)
    formset = JobEngineFormSet(request.POST or None)

    if request.method == 'POST':
        if formset.is_valid():
            formset.instance = carorder
            formset.save()
            return redirect('jobs_engine', id=carorder.pk)

    context = {'formset': formset, 'jobs': jobs, 'carorder': carorder}

    return render(request, 'jobs_engine.html', context)


# рендерит работы по КПП
def create_job_transmission(request, id):
    carorder = CarOrder.objects.get(id=id)
    jobs = JobTransmission.objects.filter(car_order=carorder)
    formset = JobTransmissionFormSet(request.POST or None)

    if request.method == 'POST':
        if formset.is_valid():
            formset.instance = carorder
            formset.save()
            return redirect('jobs_transmission', id=carorder.pk)

    context = {'formset': formset, 'jobs': jobs, 'carorder': carorder}

    return render(request, 'jobs_transmission.html', context)


def create_job_suspended(request, id):
    carorder = CarOrder.objects.get(id=id)
    jobs = JobSuspended.objects.filter(car_order=carorder)
    formset = JobSuspendedFormSet(request.POST or None)

    if request.method == 'POST':
        if formset.is_valid():
            formset.instance = carorder
            formset.save()
            return redirect('jobs_suspended', id=carorder.pk)

    context = {'formset': formset, 'jobs': jobs, 'carorder': carorder}

    return render(request, 'jobs_suspended.html', context)


def create_job_condition(request, id):
    carorder = CarOrder.objects.get(id=id)
    jobs = JobCondition.objects.filter(car_order=carorder)
    formset = JobConditionFormSet(request.POST or None)

    if request.method == 'POST':
        if formset.is_valid():
            formset.instance = carorder
            formset.save()
            return redirect('jobs_condition', id=carorder.pk)

    context = {'formset': formset, 'jobs': jobs, 'carorder': carorder}

    return render(request, 'jobs_condition.html', context)


def create_job_electric(request, id):
    carorder = CarOrder.objects.get(id=id)
    jobs = JobElectric.objects.filter(car_order=carorder)
    formset = JobElectricFormSet(request.POST or None)

    if request.method == 'POST':
        if formset.is_valid():
            formset.instance = carorder
            formset.save()
            return redirect('jobs_electric', id=carorder.pk)

    context = {'formset': formset, 'jobs': jobs, 'carorder': carorder}

    return render(request, 'jobs_electric.html', context)