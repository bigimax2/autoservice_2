from django.forms import ModelForm, modelformset_factory, inlineformset_factory
from django import forms
from .models import CarOrder, JobEngine, JobTransmission, JobSuspended, JobCondition, JobElectric


class AllOrders(ModelForm):
    class Meta:
        model = CarOrder(id='get_car_order')
        fields = '__all__'


class Test(ModelForm):
    class Meta:
        model = CarOrder
        fields = '__all__'


class JobFormEngine(forms.ModelForm):
    class Meta:
        model = JobEngine
        fields = ('job_engine', 'price', 'job_file', 'job_image')


JobEngineFormSet = inlineformset_factory(
    CarOrder,
    JobEngine,
    form=JobFormEngine,
    min_num=1,
    extra=0,
    can_delete=False
)


class JobFormTransmission(forms.ModelForm):
    class Meta:
        model = JobTransmission
        fields = ('job_transmission', 'price', 'job_file', 'job_image')


JobTransmissionFormSet = inlineformset_factory(
    CarOrder,
    JobTransmission,
    form=JobFormTransmission,
    min_num=1,
    extra=0,
    can_delete=False
)


class JobFormSuspended(forms.ModelForm):
    class Meta:
        model = JobSuspended
        fields = ('job_suspended', 'price', 'job_file', 'job_image')


JobSuspendedFormSet = inlineformset_factory(
    CarOrder,
    JobSuspended,
    form=JobFormSuspended,
    min_num=1,
    extra=0,
    can_delete=False
)


class JobFormCondition(forms.ModelForm):
    class Meta:
        model = JobCondition
        fields = ('job_condition', 'price', 'job_file', 'job_image')


JobConditionFormSet = inlineformset_factory(
    CarOrder,
    JobCondition,
    form=JobFormCondition,
    min_num=1,
    extra=0,
    can_delete=False
)


class JobFormElectric(forms.ModelForm):
    class Meta:
        model = JobElectric
        fields = ('job_electric', 'price', 'job_file', 'job_image')


JobElectricFormSet = inlineformset_factory(
    CarOrder,
    JobElectric,
    form=JobFormElectric,
    min_num=1,
    extra=0,
    can_delete=False
)